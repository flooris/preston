<?php

namespace Flooris\Preston\Console\Commands;

use DB;
use Symfony\Component\Console\Output\OutputInterface;

class ExportCmsTranslationsCommand extends PrestashopCommand
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translations:cms-export
                            {target=./exports/cms : Target directory where the export will be placed}
                            {--id_lang=1 : Language ID that is used as the export source}
                            {--id_shop=1 : Shop ID that is used as the export source}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an export of CMS language data';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $target = $this->argument('target');
        $id_lang = (int)$this->option('id_lang');
        $id_shop = (int)$this->option('id_shop');

        // Ensure the target path always ends with a /
        $target .= ends_with($target, '/') ? '' : '/';

        // Create dir when not existing
        if( ! $this->filesystem->isDirectory($target) ) {
            if( OutputInterface::VERBOSITY_VERBOSE >= $this->verbosity ) $this->info("Creating path {$target}");

            $this->filesystem->makeDirectory($target, 0755, true);
        }

        // Get a list of the available CMS pages
        $pages = $this->getCmsPages($id_lang, $id_shop);
        $pages = collect($pages);

        // Save all pages and their data in a folder
        $pages->each(function($page) use ($target) {
            $target .= (int)$page->id_cms;

            // Create dir for the CMS page when it doesn't exist yet
            if( ! $this->filesystem->isDirectory($target) ) {
                if( OutputInterface::VERBOSITY_VERBOSE >= $this->verbosity ) $this->info("Creating path {$target}");

                $this->filesystem->makeDirectory($target);
            }

            $meta = [
                'title' => $page->meta_title,
                'description' => $page->meta_description,
                'keywords' => $page->meta_keywords,
                'friendly_url' => $page->link_rewrite,
            ];
            $meta = array_map('utf8_encode', $meta);
            $meta_json = json_encode($meta, JSON_PRETTY_PRINT);
            
            if ( ! $meta_json ) {
                throw new \Exception('Failed converting meta data array to json! ' . print_r($meta, true));
            }

            if( OutputInterface::VERBOSITY_VERBOSE >= $this->verbosity ) $this->info("Saving MetaData for CMS {$page->id_cms}");
            file_put_contents(
                $target . '/metadata.json',
                $meta_json
            );

            if( OutputInterface::VERBOSITY_VERBOSE >= $this->verbosity ) $this->info("Saving Content for CMS {$page->id_cms}");
            file_put_contents(
                $target . '/content.htm',
                $page->content
            );
        });
    }

    protected function getCmsPages($id_lang, $id_shop)
    {
        $sql = '
            SELECT cms_lang.id_cms, meta_title, meta_description, meta_keywords, content, link_rewrite
            FROM '._DB_PREFIX_.'cms_lang AS cms_lang
            JOIN '._DB_PREFIX_.'cms_shop AS cms_shop ON (cms_shop.id_cms = cms_lang.id_cms)
            WHERE cms_lang.id_lang = '.(int)$id_lang.'
            AND cms_shop.id_shop = '.(int)$id_shop;

        $pages = Db::select($sql);

        return $pages;
    }
}
