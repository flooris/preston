<?php

namespace Flooris\Preston\Console\Commands;


use DB;
use Flooris\Preston\Exceptions\EmptyFolderException;
use Flooris\Preston\Exceptions\InvalidFolderException;
use Flooris\Preston\Exceptions\InvalidJsonException;

class ImportCmsTranslationsCommand extends PrestashopCommand
{

    const CONTENT_FILENAME = 'content.htm';
    const METADATA_FILENAME = 'metadata.json';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translations:cms-import
                            {path}
                            {--id_lang=1 : Language ID that will be inserted/updated}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import translated CMS files into database';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id_lang = (int)$this->option('id_lang');

        // Collect all the available translations from the given path
        $path = $this->argument('path');

        // Checks for path existence and readability
        $this->validatePathExistence($path);
        $this->validatePathReadability($path);

        // Get a list of the subdirectories existing in the path
        $directories = collect(
            $this->filesystem->directories($path)
        );

        // Check if at least 1 subdirectory is found
        if( 0 == $directories->count()) {
            throw new EmptyFolderException("No subdirectories found in {$path}.");
        }

        $this->output->writeln("<info>CMS translations importing for id_lang: {$id_lang} path: {$path}</info>");

        // Get the contents of the available translated files
        $directories->each(function($directory) use ($id_lang) {
            // Get the last part of the path, containing the category id
            $directory_parts = explode(DIRECTORY_SEPARATOR, $directory);
            $id_cms = end($directory_parts);

            // Ensure the category ID is an integer
            if( ! preg_match('/^\d+$/', $id_cms) ) {
                throw new InvalidFolderException("{$directory} is not a valid path for a CMS (ID missing in path).");
            }

            $id_cms = (int)$id_cms;

            // Get the translated files in this path
            $translations = collect(
                $this->filesystem->files($directory)
            );

            // Collect a set with the translated fields for this category
            $translated_data = [];

            $translations->each(function($translation) use (&$translated_data) {
                // Get the filename for this translation file
                $filename = $this->filesystem->basename($translation);
                $contents = $this->filesystem->get($translation);

                $data = [];

                // Determine the filetype
                if( self::METADATA_FILENAME == $filename ) {
                    // Get metadata from the file
                    $contents = json_decode($contents);

                    if ( ! $contents ) {
                        throw new InvalidJsonException("{$filename} is not a valid json file for a meta translation.");
                    }

                    $data = [
                        'meta_title'        => utf8_decode($contents->title),
                        'meta_description'  => utf8_decode($contents->description),
                        'meta_keywords'     => utf8_decode($contents->keywords),
                        'link_rewrite'      => utf8_decode($contents->friendly_url),
                    ];
                } elseif( self::CONTENT_FILENAME == $filename ) {

                    // Replace weird non-breaking space with proper HTML-encoded version
                    $contents = $contents;
                    $contents = preg_replace('/\xA0/', ' ', $contents);
                    $contents = preg_replace('/\x0A/', ' ', $contents);
                    $contents = preg_replace('/\xC2/', ' ', $contents);
                    $contents = preg_replace('/~\x{00a0}~siu/', ' ', $contents);

                    $data['content'] = $contents;
                }

                $translated_data = array_merge($translated_data, $data);
            });

            // Insert/update the translation record in the database with translated data
            DB::table('cms_lang')
                ->where('id_lang', $id_lang)
                ->where('id_cms', $id_cms)
                ->update($translated_data);

            $this->output->writeln("<info>CMS translations imported for id_lang: {$id_lang} id_cms: {$id_cms}</info>");
        });

    }
}
