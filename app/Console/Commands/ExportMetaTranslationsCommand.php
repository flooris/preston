<?php

namespace Flooris\Preston\Console\Commands;

use DB;
use Symfony\Component\Console\Output\OutputInterface;

class ExportMetaTranslationsCommand extends PrestashopCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translations:meta-export
                            {target=./exports/meta : Target directory where the export will be placed}
                            {--id_lang=1 : Language ID that is used as the export source}
                            {--id_shop=1 : Shop ID that is used as the export source}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an export of META data';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $target = $this->argument('target');
        $id_lang = (int)$this->option('id_lang');
        $id_shop = (int)$this->option('id_shop');

        // Ensure the target path always ends with a /
        $target .= ends_with($target, '/') ? '' : '/';

        // Create dir when not existing
        if( ! $this->filesystem->isDirectory($target) ) {
            if( OutputInterface::VERBOSITY_VERBOSE >= $this->verbosity ) $this->info("Creating path {$target}");

            $this->filesystem->makeDirectory($target, 0755, true);
        }

        // Get a list of the available CMS pages
        $page_meta = $this->getPageMeta($id_lang, $id_shop);
        $page_meta = collect($page_meta);

        // Save all pages and their data in a folder
        $page_meta->each(function($meta) use ($target) {
            $target .= (int)$meta->id_meta . '.json';

            $data = [
                'title' => $meta->title,
                'description' => $meta->description,
                'keywords' => $meta->keywords,
                'url_rewrite' => $meta->url_rewrite,
            ];
            $data = array_map('utf8_encode', $data);
            $meta_json = json_encode($data, JSON_PRETTY_PRINT);

            if( OutputInterface::VERBOSITY_VERBOSE >= $this->verbosity ) $this->info("Saving Content for Meta {$meta->id_meta}");
            file_put_contents(
                $target,
                $meta_json
            );
        });
    }

    protected function getPageMeta($id_lang, $id_shop)
    {
        $sql = '
            SELECT id_meta, title, description, keywords, url_rewrite 
            FROM '._DB_PREFIX_.'meta_lang
            WHERE id_lang = '.(int)$id_lang .'
            AND id_shop = '.(int)$id_shop;

        $pages = Db::select($sql);

        return $pages;
    }
}
