<?php

namespace Flooris\Preston\Console\Commands;

use DB;
use Symfony\Component\Console\Output\OutputInterface;

class ExportCategoriesTranslationsCommand extends PrestashopCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translations:categories-export
                            {target=./exports/categories : Target directory where the export will be placed}
                            {--id_lang=1 : Language ID that is used as the export source}
                            {--id_shop=1 : Shop ID that is used as the export source}
                            {--extra_field=* : Extra fields that are not PrestaShop standard}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an export of Category language data';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $target = $this->argument('target');
        $id_lang = (int)$this->option('id_lang');
        $id_shop = (int)$this->option('id_shop');

        // Ensure the target path always ends with a /
        $target .= ends_with($target, '/') ? '' : '/';

        // Create dir when not existing
        if( ! $this->filesystem->isDirectory($target) ) {
            if( OutputInterface::VERBOSITY_VERBOSE >= $this->verbosity ) $this->info("Creating path {$target}");

            $this->filesystem->makeDirectory($target, 0755, true);
        }

        // Get a list of the available Categories
        $categories = $this->getCategories($id_lang, $id_shop);
        $categories = collect($categories);

        // Save all Categories and their data in a folder
        $categories->each(function($category) use ($target) {
            $target .= (int)$category->id_category;

            // Create dir for the CMS page when it doesn't exist yet
            if( ! $this->filesystem->isDirectory($target) ) {
                if( OutputInterface::VERBOSITY_VERBOSE >= $this->verbosity ) $this->info("Creating path {$target}");

                $this->filesystem->makeDirectory($target);
            }

            $meta = [
                'name' => $category->name,
                'title' => $category->meta_title,
                'description' => $category->meta_description,
                'keywords' => $category->meta_keywords,
                'friendly_url' => $category->link_rewrite,
            ];

            if( OutputInterface::VERBOSITY_VERBOSE >= $this->verbosity ) $this->info("Saving MetaData for Category {$category->id_category}");
            file_put_contents(
                $target . '/metadata.json',
                json_encode($meta, JSON_PRETTY_PRINT)
            );

            if( OutputInterface::VERBOSITY_VERBOSE >= $this->verbosity ) $this->info("Saving Description 1 for Category {$category->id_category}");
            file_put_contents(
                $target . '/description.htm',
                $category->description
            );

            // Export extra (optional) fields
            $extra_fields = collect($this->option('extra_field'));

            $extra_fields->each(function($field) use ($category, $target) {
                if( OutputInterface::VERBOSITY_VERBOSE >= $this->verbosity ) $this->info("Saving {$field} for Category {$category->id_category}");
                file_put_contents(
                    $target . '/' . $field . '.txt',
                    $category->{$field}
                );
            });
        });
    }

    protected function getCategories($id_lang, $id_shop)
    {
        $sql = '
            SELECT *
            FROM '._DB_PREFIX_.'category AS category
            JOIN '._DB_PREFIX_.'category_shop AS category_shop ON (category_shop.id_category = category.id_category)
            JOIN '._DB_PREFIX_.'category_lang AS category_lang ON (category_lang.id_category = category.id_category AND category_lang.id_shop = category_shop.id_shop)
            WHERE category_lang.id_lang = '.(int)$id_lang.'
            AND category_lang.id_shop = '.(int)$id_shop.'
            AND NOT category.is_root_category';

        $categories = DB::select($sql);


        return $categories;
    }
}
