<?php

namespace Flooris\Preston\Console\Commands;

use DB;
use Flooris\Preston\Exceptions\EmptyFolderException;
use Flooris\Preston\Exceptions\InvalidFolderException;
use Flooris\Preston\Exceptions\InvalidJsonException;

class ImportMetaTranslationsCommand extends PrestashopCommand
{

    const METADATA_FILE_REGEX = '/^(\d+)\.json/';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translations:meta-import
                            {path}
                            {--id_lang=1 : Language ID that will be inserted/updated}
                            {--id_shop=1 : Shop ID for which translations will be inserted/updated}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id_lang = (int)$this->option('id_lang');
        $id_shop = (int)$this->option('id_shop');

        // Collect all the available translations from the given path
        $path = $this->argument('path');

        // Checks for path existence and readability
        $this->validatePathExistence($path);
        $this->validatePathReadability($path);

        // Get a list of the subdirectories existing in the path
        $files = collect(
            $this->filesystem->files($path)
        );

        // Check if at least 1 subdirectory is found
        if( 0 == $files->count()) {
            throw new EmptyFolderException("No subdirectories found in {$path}.");
        }

        // Get the contents of the available translated files
        $files->each(function($file) use ($id_lang, $id_shop) {
            // Get the last part of the path, containing the category id
            $filename_parts = explode(DIRECTORY_SEPARATOR, $file);
            $filename = end($filename_parts);

            // Ensure the category ID is an integer
            if( ! preg_match(self::METADATA_FILE_REGEX, $filename, $matches) ) {
                throw new InvalidFolderException("{$file} is not a valid name for a meta translation.");
            }

            $id_meta = (int)$matches[1];
            $contents = $this->filesystem->get($file);
            $contents = json_decode($contents);

            if ( ! $contents ) {
                throw new InvalidJsonException("{$file} is not a valid json file for a meta translation.");
            }

            $contents->title        = utf8_decode($contents->title);
            $contents->description  = utf8_decode($contents->description);
            $contents->keywords     = utf8_decode($contents->keywords);
            $contents->url_rewrite  = utf8_decode($contents->url_rewrite);

            $translated_data = [
                'title' => $contents->title,
                'description' => $contents->description,
                'keywords' => $contents->keywords,
                'url_rewrite' => $contents->url_rewrite,
            ];

            // Insert/update the translation record in the database with translated data
            $attributes = [
                'id_lang' => $id_lang,
                'id_shop' => $id_shop,
                'id_meta' => $id_meta
            ];
            DB::table('meta_lang')
                ->where('id_lang', $id_lang)
                ->where('id_shop', $id_shop)
                ->where('id_meta', $id_meta)
                ->updateOrInsert($attributes, $translated_data);

            $this->output->writeln("<info>CMS translations imported for id_shop: {$id_shop} id_lang: {$id_lang} id_meta: {$id_meta}</info>");
        });

    }
}
