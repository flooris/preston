<?php

namespace Flooris\Preston\Console\Commands;

use Config;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class PrestashopCommand extends Command
{

    /** @var FileSystem $filesystem  */
    protected $filesystem;

    protected $ps_basedir = '';

    /**
     * @var array $directories_list
     * List of directories that are expected to exist in a Prestashop installation
     */
    protected $directories_list = [
        '/config',
        '/controllers',
        '/classes',
        '/modules',
        '/themes',
        '/override'
    ];

    public function __construct()
    {
        $this->filesystem = app(Filesystem::class);

        parent::__construct();
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        // Attempt to get Prestashop bearings
        $ps_basedir = $this->getPrestashopBasedir();

        // Check if a valid Prestashop root folder has been found
        if( ! $ps_basedir ) {
            throw new \Exception("No prestashop root found in current working directory");
        }

        $this->ps_basedir = $ps_basedir;

        // Load the core PrestaShop configuration
        require_once $this->ps_basedir . '/config/settings.inc.php';

        // Configure laravel to use the Prestashop database connection
        Config::set('database.connections.prestashop.host', _DB_SERVER_);
        Config::set('database.connections.prestashop.database', _DB_NAME_);
        Config::set('database.connections.prestashop.username', _DB_USER_);
        Config::set('database.connections.prestashop.password', _DB_PASSWD_);
        Config::set('database.connections.prestashop.prefix', _DB_PREFIX_);
        Config::set('database.default', 'prestashop');

        parent::execute($input, $output);
    }

    protected function getPrestashopBasedir()
    {
        // Store the current working directory
        $cwd = getcwd();

        do {
            $is_prestashop_folder = $this->isPrestaShopDirectory($cwd);
            if( ! $is_prestashop_folder ) {
                $cwd .= '/..';
                $cwd = realpath($cwd);
            }
        } while( ! $is_prestashop_folder and $cwd != '/' );

        // Check if a valid Prestashop root folder has been found
        if( ! $is_prestashop_folder ) {
            return false;
        }

        return $cwd;
    }

    protected function isPrestaShopDirectory($working_directory)
    {
        // Prepend the current working directory to all the folder checks
        foreach( $this->directories_list as $directory ) {
            $folder = realpath($working_directory . $directory);

            // Check if the folder exists
            if( ! $this->filesystem->isDirectory($folder) ) {
                return false;
            }
        }

        // One or more folders are not found
        return true;
    }

    /**
     * Ensure a given path is existing, throw an exception when it doesn't exist
     *
     * @param string $path Full path for which to validate the existence
     */
    protected function validatePathExistence($path)
    {
        if ( ! $this->filesystem->exists($path)) {
            throw new InvalidPathException("{$path} does not seem to exist.");
        }
    }

    /**
     * Ensure a given path is readable, throw an exception when it can't be read
     *
     * @param string $path Full path for which to validate the readability
     */
    protected function validatePathReadability($path)
    {
        if ( ! $this->filesystem->isReadable($path)) {
            throw new UnreadablePathException("{$path} is not readable.");
        }
    }
}
