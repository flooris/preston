<?php

namespace Flooris\Preston\Console\Commands;

use DB;
use Flooris\Preston\Exceptions\EmptyFolderException;
use Flooris\Preston\Exceptions\InvalidFolderException;

class ImportCategoriesTranslationsCommand extends PrestashopCommand
{

    const METADATA_FILENAME = 'metadata.json';
    const DESCRIPTION_FILENAME_REGEX = '/description(\d)\.htm$/';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translations:categories-import
                            {path}
                            {--id_lang=1 : Language ID that will be inserted/updated}
                            {--id_shop=1 : Shop ID for which translations will be inserted/updated}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import translated category files into database';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id_lang = (int)$this->option('id_lang');
        $id_shop = (int)$this->option('id_shop');

        // Collect all the available translations from the given path
        $path = $this->argument('path');

        // Checks for path existence and readability
        $this->validatePathExistence($path);
        $this->validatePathReadability($path);

        // Get a list of the subdirectories existing in the path
        $directories = collect(
            $this->filesystem->directories($path)
        );

        // Check if at least 1 subdirectory is found
        if( 0 == $directories->count()) {
            throw new EmptyFolderException("No subdirectories found in {$path}.");
        }

        // Get the contents of the available translated files
        $directories->each(function($directory) use ($id_lang, $id_shop) {
            // Get the last part of the path, containing the category id
            $directory_parts = explode(DIRECTORY_SEPARATOR, $directory);
            $id_category = end($directory_parts);

            // Ensure the category ID is an integer
            if( ! preg_match('/^\d+$/', $id_category) ) {
                throw new InvalidFolderException("{$directory} is not a valid path for a category (ID missing in path).");
            }

            $id_category = (int)$id_category;

            // Get the translated files in this path
            $translations = collect(
                $this->filesystem->files($directory)
            );

            // Collect a set with the translated fields for this category
            $translated_data = [];

            $translations->each(function($translation) use (&$translated_data) {
                // Get the filename for this translation file
                $filename = $this->filesystem->basename($translation);
                $contents = $this->filesystem->get($translation);

                $data = [];

                // Determine the filetype
                if( self::METADATA_FILENAME == $filename ) {
                    // Get metadata from the file
                    $contents = json_decode(utf8_decode($contents));
                    $data = [
                        'name'             => isset($contents->name) ? $contents->name : '',
                        'link_rewrite'     => isset($contents->friendly_url) ? $contents->friendly_url : '',
                        'meta_title'       => isset($contents->title) ? $contents->title : '',
                        'meta_keywords'    => isset($contents->keywords) ? $contents->keywords : '',
                        'meta_description' => isset($contents->description) ? $contents->description : '',
                    ];
                } elseif( preg_match(self::DESCRIPTION_FILENAME_REGEX, $filename, $matches) ) {
                    $field = 'description';

                    // We have a description2 and a description3 but no description1, just description
                    if( (int)$matches[1] > 1 ) {
                        $field .= $matches[1];
                    }

                    // Replace weird non-breaking space with proper HTML-encoded version
                    $contents = utf8_encode($contents);
                    $contents = preg_replace('/\xA0/', ' ', $contents);
                    $contents = preg_replace('/~\x{00a0}~siu/', ' ', $contents);

                    $data[$field] = $contents;
                }

                $translated_data = array_merge($translated_data, $data);
            });

            $this->output->writeln("<info>Category translations importing for id_shop: {$id_shop} id_lang: {$id_lang} id_category: {$id_category}</info>");

            $translated_data = array_map('utf8_encode', $translated_data);
            try {
                // Insert/update the translation record in the database with translated data
                DB::table('category_lang')
                    ->where('id_lang', $id_lang)
                    ->where('id_shop', $id_shop)
                    ->where('id_category', $id_category)
                    ->update($translated_data);

            } catch (\Exception $exception) {
                $exception_message = $exception->getMessage();
                $this->output->writeln("<error>Category translations imported for id_shop: {$id_shop} id_lang: {$id_lang} id_category: {$id_category} - {$exception_message}</error>");
            }

            $this->output->writeln("<info>Category translations imported for id_shop: {$id_shop} id_lang: {$id_lang} id_category: {$id_category}</info>");
        });

    }
}
