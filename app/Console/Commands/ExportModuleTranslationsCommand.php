<?php

namespace Flooris\Preston\Console\Commands;

use DB;
use Symfony\Component\Console\Output\OutputInterface;

class ExportModuleTranslationsCommand extends PrestashopCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translations:module-export
                            {source_language : ISO Code of the source language (nl, fr, en)}
                            {target=./exports/modules : "Target directory where the export will be placed}
                            {--id_shop=1 : Only export modules that are installed for this shop ID}
                            {--theme= : Also export translations that are found in this theme}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export translation source files for installed modules';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $target = $this->argument('target');
        $source_language = $this->argument('source_language');
        $id_shop = (int)$this->option('id_shop');
        $theme = $this->option('theme');

        // Ensure the target path always ends with a /
        $target .= ends_with($target, '/') ? '' : '/';

        // Create dir when not existing
        if( ! $this->filesystem->isDirectory($target) ) {
            if( OutputInterface::VERBOSITY_VERBOSE >= $this->verbosity ) $this->info("Creating path {$target}");

            $this->filesystem->makeDirectory($target, 0755, true);
        }

        $installed_modules = $this->getInstalledModules($id_shop);
        $installed_modules = collect($installed_modules);

        $installed_modules->each(function($module) use ($target, $source_language, $theme) {

            $name = $module->name;
            $paths = [
                '/modules/' . $name . '/translations/',
                '/modules/' . $name . '/',
            ];

            if( $theme ) {
                $paths[] = '/themes/' . $theme . '/modules/' . $name . '/translations/';
                $paths[] = '/themes/' . $theme . '/modules/' . $name . '/';
            }

            foreach($paths as $path) {
                $filename = $this->ps_basedir . $path . $source_language . '.php';

                if( $this->filesystem->exists($filename) ) {
                    $this->info("Found translation in {$path}");

                    $target_dir = $target . $path;
                    if( ! $this->filesystem->isDirectory($target) ) {
                        $this->filesystem->makeDirectory($target, 0755, true);
                    }

                    $this->filesystem->copy($filename, $target_dir . $source_language . '.php');
                }
            }
        });
    }

    protected function getInstalledModules($id_shop)
    {
        $sql = '
            SELECT module.`name` 
            FROM '._DB_PREFIX_.'module AS module
            LEFT JOIN '._DB_PREFIX_.'module_shop AS module_shop ON (module_shop.id_module = module.id_module)
            WHERE module_shop.id_shop = '.(int)$id_shop;

        $modules = DB::select($sql);

        return $modules;
    }
}
