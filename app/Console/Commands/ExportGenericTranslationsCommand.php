<?php

namespace Flooris\Preston\Console\Commands;

use Illuminate\Console\Command;

class ExportGenericTranslationsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translations:generic-export
                            {source_language : ISO Code of the source language (nl, fr, en)}
                            {target=./exports/modules : "Target directory where the export will be placed}
                            {--id_shop=1 : Only export modules that are installed for this shop ID}
                            {--theme= : Also export translations that are found in this theme}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an export of Shop Generic language data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    }
}
