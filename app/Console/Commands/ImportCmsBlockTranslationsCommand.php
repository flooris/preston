<?php

namespace Flooris\Preston\Console\Commands;


use Carbon\Carbon;
use DB;
use Flooris\Preston\Exceptions\EmptyFolderException;
use Flooris\Preston\Exceptions\InvalidFolderException;
use Flooris\Preston\Exceptions\InvalidJsonException;

class ImportCmsBlockTranslationsCommand extends PrestashopCommand
{

    const CONTENT_FILENAME = 'content.htm';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translations:cms-block-import
                            {path}
                            {--id_lang=1     : Language ID that will be inserted/updated}\
                            {--id_shop=1     : Shop ID     that will be inserted/updated}\
                            {--id_cms_block= : CMS block ID     that will be inserted/updated}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import translated Flooris CMS Block files into database';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id_lang = (int)$this->option('id_lang');
        $id_shop = (int)$this->option('id_shop');
        $id_cms_block = (int)$this->option('id_cms_block');

        // Collect all the available translations from the given path
        $path = $this->argument('path');

        // Checks for path existence and readability
        $this->validatePathExistence($path);
        $this->validatePathReadability($path);

        // Get a list of the subdirectories existing in the path
        $directories = collect(
            $this->filesystem->directories($path)
        );

        // Check if at least 1 subdirectory is found
        if( 0 == $directories->count()) {
            throw new EmptyFolderException("No subdirectories found in {$path}.");
        }

        $this->output->writeln("<info>Flooris CMS Block translations importing for id_lang: {$id_lang} and id_shop: {$id_shop} path: {$path}</info>");

        // Get the contents of the available translated files
        $directories->each(function($directory) use ($id_lang, $id_shop, $id_cms_block) {
            // Get the last part of the path, containing the category id
            $directory_parts = explode(DIRECTORY_SEPARATOR, $directory);
            $id_flooris_cmsblock = end($directory_parts);

            // Ensure the category ID is an integer
            if( ! preg_match('/^\d+$/', $id_flooris_cmsblock) ) {
                throw new InvalidFolderException("{$directory} is not a valid path for a CMS (ID missing in path).");
            }

            $id_flooris_cmsblock = (int)$id_flooris_cmsblock;

            if ($id_cms_block && $id_cms_block <> $id_flooris_cmsblock) {
                // Skip this block
                return true;
            }

            // Get the translated files in this path
            $translations = collect(
                $this->filesystem->files($directory)
            );

            // Collect a set with the translated fields for this category
            $translated_data = [];

            try {
                $translations->each(function($translation) use (&$translated_data) {
                    // Get the filename for this translation file
                    $filename = $this->filesystem->basename($translation);
                    $contents = $this->filesystem->get($translation);

                    $data = [];

                    // Determine the filetype
                    if( self::CONTENT_FILENAME == $filename ) {

                        // Replace weird non-breaking space with proper HTML-encoded version
                        $contents = utf8_decode($contents);
                        $contents = preg_replace('/\xA0/', ' ', $contents);
                        $contents = preg_replace('/~\x{00a0}~siu/', ' ', $contents);

                        $data['text'] = $contents;
                    }

                    $translated_data = array_merge($translated_data, $data);
                });

                // Insert/update the translation record in the database with translated data
                $recordQuery = DB::table('flooris_cmsblock_lang')
                    ->where('id_lang', $id_lang)
                    ->where('id_shop', $id_shop)
                    ->where('id_flooris_cmsblock', $id_flooris_cmsblock);

                $result = $recordQuery->get();

                if ($result->count() > 0) {
                    $recordQuery->update($translated_data);
                } else {
                    $this->insertNewCmsBlock($id_flooris_cmsblock, $id_shop, $id_lang, $translated_data);
                }

                $this->output->writeln("<info>CMS translations imported for id_lang: {$id_lang} id_cms: {$id_flooris_cmsblock}</info>");

            } catch (\Exception $exception) {
                $this->error("Fault in id_flooris_cmsblock: {$id_flooris_cmsblock} id_shop: {$id_shop} id_lang: {$id_lang}");// . $exception->getMessage());
//                throw $exception;
            }


        });

    }

    private function insertNewCmsBlock($id_flooris_cmsblock, $id_shop, $id_lang, $translated_data)
    {
        // Insert CMS block lang.
        DB::table('flooris_cmsblock_lang')->insert(array_merge([
            'id_lang'             => $id_lang,
            'id_shop'             => $id_shop,
            'id_flooris_cmsblock' => $id_flooris_cmsblock,
        ], $translated_data));

        $cmsShopQuery = DB::table('flooris_cmsblock_shop')
                          ->where('id_shop', $id_shop)
                          ->where('id_flooris_cmsblock', $id_flooris_cmsblock);

        if ($cmsShopQuery->get()->count() === 0) {
            // Insert CMS block shop.
            DB::table('flooris_cmsblock_shop')->insert([
                'id_shop'             => $id_shop,
                'id_flooris_cmsblock' => $id_flooris_cmsblock,
            ]);
        }

        $cmsBlockQuery = DB::table('flooris_cmsblock')
                           ->where('id_flooris_cmsblock', $id_flooris_cmsblock);

        if ($cmsBlockQuery->get()->count() === 0) {
            // Insert CMS block.
            DB::table('flooris_cmsblock')->insert([
                'id_flooris_cmsblock' => $id_flooris_cmsblock,
                'date_add'            => Carbon::now(),
                'date_upd'            => Carbon::now(),
            ]);
        }

        return;
    }
}
