<?php

namespace Flooris\Preston\Console\Commands;

use DB;
use Log;
use GuzzleHttp\Client;
use GuzzleHttp\TransferStats;
use Illuminate\Console\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CheckProductsCommand extends PrestashopCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:products
                            {--id_product= : Product ID}
                            {--id_lang=1 : Language ID that is used as the export source}
                            {--id_shop=1 : Shop ID that is used as the export source}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check product pages';

    protected $input, $output;

    /**
     * Execute the console command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return mixed
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        parent::execute($input, $output);

        $this->input = $input;
        $this->output = $output;
    }

    public function handle()
    {
        $id_lang    = (int)$this->option('id_lang');
        $id_shop    = (int)$this->option('id_shop');
        $id_product = (int)$this->option('id_product');

        $pages = [];

        $shop_url = $this->getShopUrl($id_shop);
        $client = new Client(['base_uri' => $shop_url]);

        $products = $this->getProducts($id_lang, $id_shop, $id_product);

        $progress = new ProgressBar($this->output, iterator_count($products));
        $progress->start();

        $products->each(function($row) use ($shop_url, $progress, $client, &$pages) {
            $id_product = (int)$row->id_product;
            $name = $row->name;
            $product_uri = $row->product_uri;
            $product_url = $shop_url . '/' . $product_uri;



            $client->request('HEAD', $product_uri, [
                'http_errors' => false,
                'on_stats' => function (TransferStats $stats) use ($product_url, $id_product, &$pages) {

                    if ($stats->hasResponse()) {

                        $response_http_status_code = (int)$stats->getResponse()->getStatusCode();
                        if ( ! isset($pages[$response_http_status_code]) ) {
                            $pages[$response_http_status_code] = [];
                        }

                        if ($response_http_status_code <> 200) {
                            $pages[$response_http_status_code][] = $product_url;
                        }

                        Log::info("HTTP response: {$response_http_status_code} - id_product: {$id_product} - URL: {$product_url}");

                    } else {

                        Log::warning("Checking url: {$product_url} for id_product: {$id_product} - NO RESPONSE !!!");
                    }
                }
            ]);

            $progress->advance();
        });

        Log::info(print_r($pages, true));
    }

    protected function getProducts($id_lang, $id_shop, $id_product = null)
    {
        $sql = "
            SELECT 
                product.`id_product`,
                product_lang.`name`,
                product_lang.`link_rewrite` as `product_link_rewrite`,
                CONCAT(category_lang.`link_rewrite`, '/', product_lang.`link_rewrite`, '.html') AS `product_uri` 
            FROM "._DB_PREFIX_."product AS product
            JOIN "._DB_PREFIX_."product_shop AS product_shop ON (product_shop.id_product = product.id_product)
            JOIN "._DB_PREFIX_."product_lang AS product_lang ON (product_lang.id_product = product.id_product AND product_lang.id_shop = product_shop.id_shop)
            JOIN "._DB_PREFIX_."category_lang AS category_lang ON (category_lang.id_shop = product_lang.id_shop AND category_lang.id_lang = product_lang.id_lang AND category_lang.id_category = product_shop.id_category_default)
            WHERE product_lang.id_lang = ".(int)$id_lang."
            AND   product_lang.id_shop = ".(int)$id_shop."
            ";

        if ($id_product > 0) {
            $sql .= '
            AND product.`id_product` = ' . (int)$id_product;
        } else {
            $sql .= '
            AND product_shop.`active` = 1';
        }

        $products = DB::select($sql);
        $products = collect($products);

        return $products;
    }

    protected function getShopUrl($id_shop)
    {
        $ssl_enabled_result = DB::selectOne("
            SELECT `value`
            FROM `"._DB_PREFIX_."configuration`
            WHERE `name` = 'PS_SSL_ENABLED'
            AND (`id_shop` IS NULL OR `id_shop` = {$id_shop})");
        $ssl_enabled = (bool)$ssl_enabled_result->value;

        $is_https = false;
        /** ToDo: Check if the shop is SSL */

        $shop_url_result = DB::selectOne("
            SELECT `domain`, `domain_ssl` 
            FROM `"._DB_PREFIX_."shop_url` 
            WHERE `id_shop` = {$id_shop} 
            AND `main` = 1 
            AND `active` = 1");

        $shop_url = ($ssl_enabled ? 'https://' : 'http://');
        $shop_url .= ($ssl_enabled ? $shop_url_result->domain_ssl : $shop_url_result->domain);

        return $shop_url;
    }
}
