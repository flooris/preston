<?php

namespace Flooris\Preston\Console\Commands;

use DB;
use Symfony\Component\Console\Output\OutputInterface;

class ExportCmsBlockTranslationsCommand extends PrestashopCommand
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translations:cms-block-export
                            {target=./exports/cms-block : Target directory where the export will be placed}
                            {--id_lang=1 : Language ID that is used as the export source}
                            {--id_shop=1 : Shop ID that is used as the export source}
                            {--id_cms_block= : CMS Block ID}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an export of Flooris CMS Block language data';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $target = $this->argument('target');
        $id_lang = (int)$this->option('id_lang');
        $id_shop = (int)$this->option('id_shop');
        $id_cms_block = (int)$this->option('id_cms_block');

        // Ensure the target path always ends with a /
        $target .= ends_with($target, '/') ? '' : '/';

        // Create dir when not existing
        if( ! $this->filesystem->isDirectory($target) ) {
            if( OutputInterface::VERBOSITY_VERBOSE >= $this->verbosity ) $this->info("Creating path {$target}");

            $this->filesystem->makeDirectory($target, 0755, true);
        }

        // Get a list of the available CMS pages
        $pages = $this->getFloorisCmsBlocks($id_lang, $id_shop);
        $pages = collect($pages);

        // Save all pages and their data in a folder
        $pages->each(function($cms_block) use ($target, $id_cms_block) {
            $id_flooris_cmsblock = (int)(int)$cms_block->id_flooris_cmsblock;

            $target .= $id_flooris_cmsblock;

            if ($id_cms_block && $id_cms_block <> $id_flooris_cmsblock) {
                return true;
            }

            // Create dir for the CMS page when it doesn't exist yet
            if( ! $this->filesystem->isDirectory($target) ) {
                if( OutputInterface::VERBOSITY_VERBOSE >= $this->verbosity ) $this->info("Creating path {$target}");

                $this->filesystem->makeDirectory($target);
            }

            if( OutputInterface::VERBOSITY_VERBOSE >= $this->verbosity ) $this->info("Saving Content for Flooris CMS Block {$cms_block->id_flooris_cmsblock}");
            file_put_contents(
                $target . '/content.htm',
                $cms_block->text
            );
        });
    }

    protected function getFloorisCmsBlocks($id_lang, $id_shop)
    {
        $sql = '
            SELECT cms_lang.`id_flooris_cmsblock`, cms_lang.`text`
            FROM '._DB_PREFIX_.'flooris_cmsblock_lang AS cms_lang
            JOIN '._DB_PREFIX_.'flooris_cmsblock_shop AS cms_shop ON (cms_shop.id_flooris_cmsblock = cms_lang.id_flooris_cmsblock AND cms_shop.id_shop = cms_lang.id_shop)
            WHERE cms_lang.id_lang = '.(int)$id_lang.'
            AND   cms_lang.id_shop = '.(int)$id_shop;

        $pages = Db::select($sql);

        return $pages;
    }
}
