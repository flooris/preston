<?php

namespace Flooris\Preston\Console;

use Flooris\Preston\Console\Commands\CheckProductsCommand;
use Flooris\Preston\Console\Commands\ExportCmsBlockTranslationsCommand;
use Flooris\Preston\Console\Commands\ImportCmsBlockTranslationsCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Flooris\Preston\Console\Commands\ExportCmsTranslationsCommand;
use Flooris\Preston\Console\Commands\ImportCmsTranslationsCommand;
use Flooris\Preston\Console\Commands\ExportMetaTranslationsCommand;
use Flooris\Preston\Console\Commands\ImportMetaTranslationsCommand;
use Flooris\Preston\Console\Commands\ExportModuleTranslationsCommand;
use Flooris\Preston\Console\Commands\ExportCategoriesTranslationsCommand;
use Flooris\Preston\Console\Commands\ImportCategoriesTranslationsCommand;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ExportCategoriesTranslationsCommand::class,
        ExportCmsTranslationsCommand::class,
        ExportMetaTranslationsCommand::class,
        ExportModuleTranslationsCommand::class,
        ImportCategoriesTranslationsCommand::class,
        ImportCmsTranslationsCommand::class,
        ImportMetaTranslationsCommand::class,
        CheckProductsCommand::class,
        ExportCmsBlockTranslationsCommand::class,
        ImportCmsBlockTranslationsCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
