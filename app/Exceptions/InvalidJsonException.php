<?php

namespace Flooris\Preston\Exceptions;

use InvalidArgumentException;
use Throwable;

class InvalidJsonException extends InvalidArgumentException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $message .= PHP_EOL . "json last error message: " . PHP_EOL . json_last_error_msg();
        return parent::__construct($message, $code, $previous);
    }
}
