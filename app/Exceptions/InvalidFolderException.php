<?php

namespace Flooris\Preston\Exceptions;

use InvalidArgumentException;

class InvalidFolderException extends InvalidArgumentException
{
    //
}
