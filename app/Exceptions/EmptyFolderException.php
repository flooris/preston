<?php

namespace Flooris\Preston\Exceptions;

use InvalidArgumentException;

class EmptyFolderException extends InvalidArgumentException
{
    //
}
