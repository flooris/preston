<?php

namespace Flooris\Preston\Exceptions;

use InvalidArgumentException;

class InvalidPathException extends InvalidArgumentException
{
    //
}
