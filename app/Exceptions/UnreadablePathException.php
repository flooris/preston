<?php

namespace Flooris\Preston\Exceptions;

use InvalidArgumentException;

class UnreadablePathException extends InvalidArgumentException
{
    //
}
